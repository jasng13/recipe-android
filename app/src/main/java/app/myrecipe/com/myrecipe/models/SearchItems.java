package app.myrecipe.com.myrecipe.models;

import android.widget.CheckBox;
import android.widget.TextView;

public class SearchItems {

    private CheckBox checkBox ;
    private String item ;

    public SearchItems(String item, CheckBox checkBox ) {
        this.checkBox = checkBox ;
        this.item = item ;

    }

    public CheckBox getCheckBox() {
        return checkBox;
    }
    public void setCheckBox(CheckBox checkBox) {
        this.checkBox = checkBox;
    }
    public String getString() {
        return item;
    }
    public void setString(String item) {
        this.item = item;
    }
}
