package app.myrecipe.com.myrecipe.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import app.myrecipe.com.myrecipe.R;
import app.myrecipe.com.myrecipe.models.RecipesItems;
import app.myrecipe.com.myrecipe.models.SearchItems;

public class RecipesListAdapter extends BaseAdapter {

    Context context;
    List<RecipesItems> rowItem;

    public RecipesListAdapter(Context context, List<RecipesItems> rowItem) {
        this.context = context;
        this.rowItem = rowItem;

    }

    @Override
    public int getCount() {

        return rowItem.size();
    }

    @Override
    public Object getItem(int position) {

        return rowItem.get(position);
    }

    @Override
    public long getItemId(int position) {

        return rowItem.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.list_recipes_items, null);
        }

        ImageView imgIcon = (ImageView) convertView.findViewById(R.id.list_image);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
        TextView textDesc = (TextView) convertView.findViewById(R.id.desc);

        RecipesItems row_pos = rowItem.get(position);
        // setting the image resource and title
        imgIcon.setImageResource(row_pos.getImg());
        txtTitle.setText(row_pos.getItem());
        textDesc.setText(row_pos.getDesc());

        return convertView;

    }

}
