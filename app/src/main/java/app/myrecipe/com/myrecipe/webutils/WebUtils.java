package app.myrecipe.com.myrecipe.webutils;

import android.text.TextUtils;
import android.util.Log;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;

public class WebUtils {

	private static String serverURL = "http://172.18.37.107:8080/Recipe";
public static String getURL(String url) throws IOException {
	System.out.println(InetAddress.getLocalHost().getHostAddress());
	Log.i("ip", InetAddress.getLocalHost().getHostAddress());
//	String serverU="http://"+InetAddress.getLocalHost().getHostAddress()+":8080/TheServer/";
    HttpURLConnection conn = (HttpURLConnection) new URL(serverURL + url).openConnection();
//	HttpURLConnection conn = (HttpURLConnection) new URL(serverU + url).openConnection();
    conn.setRequestMethod("GET");  // actually unnecessary since GET is the default
    conn.connect();
/*
    webUtils
    DocOrder
    DPatientInfo
    NCharts
    NDownloadResults
    NPatientCOmment
    Npinfo_tr
*/
    InputStream is = null;
    try {
        return readStream(conn.getInputStream());
    } finally {
        if (is != null) {
            is.close();
        }
        conn.disconnect();
    }
}

public static String postURL(String url, String... parameters) throws IOException {
//	String serverU="http://"+InetAddress.getLocalHost().getHostAddress()+":8080/TheServer/";
	HttpClient httpClient=new DefaultHttpClient();
    HttpURLConnection conn = (HttpURLConnection) new URL(serverURL + url).openConnection();
//    HttpURLConnection conn = (HttpURLConnection) new URL(serverU + url).openConnection();

    conn.setRequestMethod("POST");  // actually unnecessary since setDoOutput(true) makes it POST
    conn.setDoOutput(true);  // allows data to be passed
    //httpClient.execute(conn);
    OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
    try {
        writer.write(TextUtils.join("&", parameters));
        writer.flush();
    } finally {
    	
        writer.close();
    }

    conn.connect();

    InputStream is = null;
    try {
        return readStream(conn.getInputStream());
        
    }
//    catch(Exception e){
//    	return e.getMessage();
//    	System.out.println(e);
//    }  
    finally {
        if (is != null) {
            is.close();
        }
        conn.disconnect();
    }
}


private static String readStream(InputStream stream) throws IOException {
    BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
    try {
        StringBuilder sb = new StringBuilder();
        for (String line = reader.readLine(); line != null; line = reader.readLine()) {
            sb.append(line);
            sb.append('\n');  // this might add an extra newline at the end
        }
        return sb.toString();
    } finally {
        reader.close();
    }
}
}