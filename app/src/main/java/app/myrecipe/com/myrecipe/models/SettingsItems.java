package app.myrecipe.com.myrecipe.models;

import android.widget.CheckBox;
import android.widget.TextView;

public class SettingsItems {

    private CheckBox checkBox ;
    private String item ;

    public SettingsItems(String item) {
        this.item = item ;

    }


    public String getString() {
        return item;
    }
    public void setString(String item) {
        this.item = item;
    }
}
