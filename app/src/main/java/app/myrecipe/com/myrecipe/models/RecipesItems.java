package app.myrecipe.com.myrecipe.models;

import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

public class RecipesItems {

    private String desc ;
    private String item ;
    private int img;

    public RecipesItems(String item, String desc, int img) {
        this.desc = desc ;
        this.item = item ;
        this.img = img ;
    }


    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }


    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }
}
